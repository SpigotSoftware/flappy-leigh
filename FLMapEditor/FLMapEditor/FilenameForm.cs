﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FLMapEditor
{
    public partial class FilenameForm : Form
    {
        public FilenameForm()
        {
            InitializeComponent();
        }

        private void Button_Finish_Click(object sender, EventArgs e)
        {
            //Filename += textBox_Filename.Text;
            this.Close();
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
