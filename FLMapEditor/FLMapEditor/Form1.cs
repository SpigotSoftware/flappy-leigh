﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace FLMapEditor
{
    public partial class FLME : Form
    {
        private int column;
        private int row;
        private int i;
        private int x;
        private string filename = "../../../flappy-leigh/FlappyLeigh/Binary/Content/";
        private string name;
        Image aliengreen;
        Image tower;
        PictureBox[,] arryTiles;
        StreamWriter stream;

        public FLME()
        {
            InitializeComponent();
        }

        private void FLME_Load(object sender, EventArgs e)
        {
            // Load values into attributes on startup
            this.StartPosition = FormStartPosition.Manual;
            this.Size = new Size(1280, 820);
            column = 46;
            row = 30;
            arryTiles = new PictureBox[row,column];
            stream = null;
            aliengreen = Image.FromFile("../aliengreen.png");
            tower = Image.FromFile("../tower.png");

            // Setting the image and size for the background
            pictureBox_Background.Load("../FLMapEditor/Properties/FlappyLeighBackgroundBitMap.bmp");
            pictureBox_Background.Size = new Size(1080, 720);

            // Seetting the buttons/rtb relative to the background set above
            button_Import.Location = new Point(pictureBox_Background.Location.X + pictureBox_Background.Size.Width - 2*button_Import.Size.Width, pictureBox_Background.Location.Y + pictureBox_Background.Size.Height + 5);
            button_Export.Location = new Point(pictureBox_Background.Location.X + pictureBox_Background.Size.Width - button_Export.Size.Width + 24, button_Import.Location.Y);
            button_Clear.Location = new Point(pictureBox_Background.Location.X, button_Import.Location.Y);
            textBox_Filename.Location = new Point(button_Import.Location.X - textBox_Filename.Size.Width - 5, button_Import.Location.Y + 8);
            lable_filename.Location = new Point(textBox_Filename.Location.X - lable_filename.Size.Width - 5, button_Import.Location.Y + 8);
            // Uses a double for loop to create a picture box for the tower for each row x column spot
            // Then it uses the outside loop to create a check box for every row
            for (i = 0; i < row; i++)
            {
                for (x = 0; x < column; x++)
                {
                    // Creates a PictureBox object and adds it to the form
                    arryTiles[i, x] = new PictureBox();
                    arryTiles[i, x].Size = new Size(24, 24);
                    arryTiles[i, x].Image = tower;
                    arryTiles[i, x].Location = new Point(pictureBox_Background.Location.X + (x * 24), pictureBox_Background.Location.Y + (i * 24));
                    arryTiles[i, x].BackColor = Color.Black;
                    arryTiles[i, x].Visible = false;
                    arryTiles[i, x].Click += new EventHandler(Picturebox_Visible);
                    pictureBox_Background.Click += new EventHandler(Background_Click);
                    this.Controls.Add(arryTiles[i, x]);
                    arryTiles[i, x].BringToFront();
                }
            }
        }

        private void button_Import_Click(object sender, EventArgs e)
        {
            name = textBox_Filename.Text;
            StreamReader stream = null;
            try
            {
                stream = new StreamReader(filename + name);
                string line = "";
                int b = 0;
                while ((line = stream.ReadLine()) != null)
                {
                    for (int a = 0; a < line.Length; a++)
                    {
                        if (line[a] == '.')
                            arryTiles[b, a].Visible = false;
                        else if (line[a] == 'i')
                        {
                            arryTiles[b, a].Visible = true;
                            arryTiles[b, a].Image = tower;
                        }
                        else if (line[a] == 't')
                        {
                            arryTiles[b, a].Visible = true;
                            arryTiles[b, a].Image = aliengreen;
                        }
                    }
                    b++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }

        private void button_Export_Click(object sender, EventArgs e)
        {
            name = textBox_Filename.Text;

			stream = new StreamWriter(filename + name);
            for(int a = 0; a < row; a++)
            {
                for(int b = 0; b < column; b++)
                {
                    if (arryTiles[a, b].Visible == false)
                        stream.Write(".");
                    else if (arryTiles[a, b].Image == tower)
                        stream.Write("i");
                    else if (arryTiles[a, b].Image == aliengreen)
                        stream.Write("t");
                }
                stream.WriteLine();
            }
            stream.Close();
        }

        private void Picturebox_Visible(object sender, System.EventArgs e)
        {
            PictureBox pb;
            pb = (PictureBox)sender;
            if (pb.Visible && pb.Image == tower)
                pb.Image = aliengreen;
            else if (pb.Visible && pb.Image == aliengreen)
            {
                pb.Image = tower;
                pb.Visible = false;
            }
        }

        private void Background_Click(object sender, System.EventArgs e)
        {
            foreach (var variable in this.Controls)
                if (variable is PictureBox)
                {
                    PictureBox pb = (PictureBox) variable;
                    if(pb.Bounds.Contains(PointToClient(MousePosition)))
                        pb.Visible = true;
                }

        }

        private void button_Clear_Click(object sender, EventArgs e)
        {
            for (int a = 0; a < row; a++)
            {
                for (int b = 0; b < column; b++)
                {
                    arryTiles[a, b].Visible = false;
                }
            }
        }
    }
}
