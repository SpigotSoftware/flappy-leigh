﻿namespace FLMapEditor
{
    partial class FilenameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Filename = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Button_Finish = new System.Windows.Forms.Button();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_Filename
            // 
            this.textBox_Filename.Location = new System.Drawing.Point(83, 50);
            this.textBox_Filename.Name = "textBox_Filename";
            this.textBox_Filename.Size = new System.Drawing.Size(556, 36);
            this.textBox_Filename.TabIndex = 0;
            this.textBox_Filename.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Filename";
            // 
            // Button_Finish
            // 
            this.Button_Finish.Location = new System.Drawing.Point(407, 108);
            this.Button_Finish.Name = "Button_Finish";
            this.Button_Finish.Size = new System.Drawing.Size(113, 47);
            this.Button_Finish.TabIndex = 2;
            this.Button_Finish.Text = "Finish";
            this.Button_Finish.UseVisualStyleBackColor = true;
            this.Button_Finish.Click += new System.EventHandler(this.Button_Finish_Click);
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Location = new System.Drawing.Point(526, 108);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(113, 47);
            this.Button_Cancel.TabIndex = 3;
            this.Button_Cancel.Text = "Cancel";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // FilenameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 167);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Finish);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_Filename);
            this.Name = "FilenameForm";
            this.Text = "FilenameForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox textBox_Filename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Button_Finish;
        private System.Windows.Forms.Button Button_Cancel;
    }
}