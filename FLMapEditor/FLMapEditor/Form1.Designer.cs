﻿namespace FLMapEditor
{
    partial class FLME
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Import = new System.Windows.Forms.Button();
            this.button_Export = new System.Windows.Forms.Button();
            this.pictureBox_Background = new System.Windows.Forms.PictureBox();
            this.button_Clear = new System.Windows.Forms.Button();
            this.lable_filename = new System.Windows.Forms.Label();
            this.textBox_Filename = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Background)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Import
            // 
            this.button_Import.Location = new System.Drawing.Point(985, 763);
            this.button_Import.Margin = new System.Windows.Forms.Padding(4);
            this.button_Import.Name = "button_Import";
            this.button_Import.Size = new System.Drawing.Size(113, 47);
            this.button_Import.TabIndex = 1;
            this.button_Import.Text = "Import";
            this.button_Import.UseVisualStyleBackColor = true;
            this.button_Import.Click += new System.EventHandler(this.button_Import_Click);
            // 
            // button_Export
            // 
            this.button_Export.Location = new System.Drawing.Point(1107, 763);
            this.button_Export.Margin = new System.Windows.Forms.Padding(4);
            this.button_Export.Name = "button_Export";
            this.button_Export.Size = new System.Drawing.Size(113, 47);
            this.button_Export.TabIndex = 24;
            this.button_Export.Text = "Export";
            this.button_Export.UseVisualStyleBackColor = true;
            this.button_Export.Click += new System.EventHandler(this.button_Export_Click);
            // 
            // pictureBox_Background
            // 
            this.pictureBox_Background.Image = global::FLMapEditor.Properties.Resources.FlappyLeighBackgroundBitMap;
            this.pictureBox_Background.Location = new System.Drawing.Point(139, 15);
            this.pictureBox_Background.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox_Background.Name = "pictureBox_Background";
            this.pictureBox_Background.Size = new System.Drawing.Size(1080, 720);
            this.pictureBox_Background.TabIndex = 35;
            this.pictureBox_Background.TabStop = false;
            // 
            // button_Clear
            // 
            this.button_Clear.Location = new System.Drawing.Point(139, 763);
            this.button_Clear.Margin = new System.Windows.Forms.Padding(4);
            this.button_Clear.Name = "button_Clear";
            this.button_Clear.Size = new System.Drawing.Size(113, 47);
            this.button_Clear.TabIndex = 36;
            this.button_Clear.Text = "Clear";
            this.button_Clear.UseVisualStyleBackColor = true;
            this.button_Clear.Click += new System.EventHandler(this.button_Clear_Click);
            // 
            // lable_filename
            // 
            this.lable_filename.AutoSize = true;
            this.lable_filename.Location = new System.Drawing.Point(722, 778);
            this.lable_filename.Name = "lable_filename";
            this.lable_filename.Size = new System.Drawing.Size(69, 17);
            this.lable_filename.TabIndex = 37;
            this.lable_filename.Text = "Filename:";
            // 
            // textBox_Filename
            // 
            this.textBox_Filename.Location = new System.Drawing.Point(797, 775);
            this.textBox_Filename.Name = "textBox_Filename";
            this.textBox_Filename.Size = new System.Drawing.Size(181, 22);
            this.textBox_Filename.TabIndex = 38;
            // 
            // FLME
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1325, 825);
            this.Controls.Add(this.textBox_Filename);
            this.Controls.Add(this.lable_filename);
            this.Controls.Add(this.button_Clear);
            this.Controls.Add(this.pictureBox_Background);
            this.Controls.Add(this.button_Export);
            this.Controls.Add(this.button_Import);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FLME";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Flappy Leigh Map Editor";
            this.Load += new System.EventHandler(this.FLME_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Background)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Import;
        private System.Windows.Forms.Button button_Export;
        private System.Windows.Forms.PictureBox pictureBox_Background;
        private System.Windows.Forms.Button button_Clear;
        private System.Windows.Forms.Label lable_filename;
        private System.Windows.Forms.TextBox textBox_Filename;
    }
}

