﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace FlappyLeigh
{
    enum ObstacleSprites { Tower, Lego, Shield, AddThree } //to provide an easy switch later on 
    class Obstacles : GameObject //Carter
    {
        Vector2 obstacleVec; //Vec2 for the scrolling of the obstacles
       
        ObstacleSprites choice;

        public Obstacles(ObstacleSprites spriteChoice) //sets the enum
        {
            choice = spriteChoice;
        }

        public Vector2 ObstacleVec //property
        {
            get { return obstacleVec; }
            set { obstacleVec = value; }
        }
    }
}
