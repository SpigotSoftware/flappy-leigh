﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlappyLeigh
{
    class Update
    {
        //the purpose of this class was to consolidate much of the Game1.Update method into a separate class to reduce its length
        //having it caused the program to crash or freeze in different places, and there was not enough time to fix it
        //the class is being left in due to problems that arose from it's absence causing the game to not run
    }
}
