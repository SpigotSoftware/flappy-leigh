﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace FlappyLeigh
{
    class AddThree:Powerup
    {
        public Rectangle rectangle;
        public Texture2D texture2D;
        public int SIZE = 32;
        public bool passed;
        Random rand1 = new Random();
        public bool addThreeActive;

        public AddThree(ObstacleSprites spriteChoice, Rectangle rec1)
            : base(spriteChoice)
        {
            rectangle = rec1;
            passed = false;
        }

        public void AddThreeMove(AddThree add, Player p1) //Moves these individial add three pieces
        {
            add.rectangle.X -= (int)(2 + p1.Score / 3);

            if (add.rectangle.X <= -add.SIZE) //tile wrap
            {
                add.rectangle.X = 1440;
                add.passed = false;
                add.addThreeActive = true;
                add.rectangle.Y = rand1.Next(60, 900);
            }
        }

        public void ScoreAddThree(Player p1, Player p2, AddThree ad3) //Adds three to the player's score based on who hit it first. If both hit it randomly determines one player
        {
            if (p1.Rect.Intersects(ad3.rectangle) && !p2.Rect.Intersects(ad3.rectangle))
            {
                p1.Score = p1.Score + 3;
                addThreeActive = false;
            }
            
            if(p2.Rect.Intersects(ad3.rectangle) && !p1.Rect.Intersects(ad3.rectangle))
            {
                p2.Score = p2.Score + 3;
                addThreeActive = false;
            }

            if (p1.Rect.Intersects(ad3.rectangle) && p2.Rect.Intersects(ad3.rectangle))
            {
                int i = rand1.Next(0, 2);
                if (i == 0)
                {
                    p1.Score = p1.Score + 3;
                }
                if (i == 1)
                {
                    p2.Score = p2.Score + 3;
                }
                addThreeActive = false;
            }
        }
    }
}
