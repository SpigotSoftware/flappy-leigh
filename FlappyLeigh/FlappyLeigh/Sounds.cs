﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;

namespace FlappyLeigh
{
    class Sounds
    {
        public  SoundEffect laugh;
        public  SoundEffect yeah;
        public  SoundEffect wtf;
        public  SoundEffect ugh;
        public  SoundEffect backGroundNoise;
        public  SoundEffectInstance laughInstance;
        public  SoundEffectInstance yeahInstance;
        public  SoundEffectInstance wtfInstance;
        public  SoundEffectInstance ughInstance;
    }
}
