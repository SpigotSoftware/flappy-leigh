﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace FlappyLeigh
{
    enum PowerupSprites { Shield }
    class Powerup : Obstacles
    {
        Vector2 powerupVec; //Vec2 for the scrolling of the powerups
        bool shieldBool;
        PowerupSprites choice;

        public Powerup(ObstacleSprites spriteChoice)
            : base(spriteChoice)
        {

        }

        public Vector2 PowerupVec
        {
            get { return powerupVec; }
            set { powerupVec = value; }
        }

        public void shield(Player p1)
        {
            if (this.Collision(p1) && choice == PowerupSprites.Shield)
            {
                shieldBool = true;
            }
        }
    }
}
