﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;


namespace FlappyLeigh
{
    abstract class GameObject //Carter
    {
        protected int size; //size of the object
        protected Texture2D sprite; //sprite
        protected Rectangle rect; //rectangle
        protected KeyboardState kState;
        protected KeyboardState prevKState;

        public bool Collision(GameObject obj) //check for two intersecting rects
        {
            return rect.Intersects(obj.rect);
        }

        public virtual void Update()
        {
        }

        public virtual void Draw(SpriteBatch sprites)
        {
            sprites.Draw(sprite, rect, Color.White);
        }

        public bool SingleKeyPress(Keys key) //register a single press
        {
            return (kState.IsKeyDown(key) && prevKState.IsKeyUp(key));
        }

        public Rectangle Rect //properties form here down
        {
            get { return rect; }
            set { rect = value; }
        }

        public int X
        {
            get { return rect.X; }
            set { rect.X = value; }
        }

        public int Y
        {
            get { return rect.Y; }
            set { rect.Y = value; }
        }

        public int Size
        {
            get { return size; }
            set { size = value; }
        }

        public Texture2D Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }

        public KeyboardState KState
        {
            get { return kState; }
            set { kState = value; }
        }

        public KeyboardState PrevKState
        {
            get { return prevKState; }
            set { prevKState = value; }
        }
    }
}
