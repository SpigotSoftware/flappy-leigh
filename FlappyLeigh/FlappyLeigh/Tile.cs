﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.IO;
namespace FlappyLeigh
{
    class Tile
    {
        public Rectangle rectangle;     //rectangle variable
        public String fileName;         //what kind of tile is it?
        public Texture2D texture2D;     //image texture
        public int SIZE = 32;           //fixed size of each tile
        public bool passed;             //have the players scored for this tile?

        public Tile(char nameInput)
        {
            TileLoad(nameInput);
            rectangle = new Rectangle(0, 0, SIZE, SIZE);
            passed = false;
        }

        public void TileLoad(char letter)
        {
            switch (letter) //will later include provisions 
            {
                case 'i':
                    fileName = "Textures/tower";
                    break;
                case 't':
                    fileName = "Textures/aliengreen";
                    break;
                default:
                    fileName = "Textures/empty";
                    break;
            }
        }

        public bool CheckIfWas(Tile tile, Player p1, GameState state)
        {

            if (tile.fileName == "Textures/tower" ||
                tile.fileName == "Textures/aliengreen") //return if the player intersects with the object
            {
                if (tile.rectangle.Intersects(p1.Rect))
                {
                    return true;
                }
            }
            return false;
        }

        public void CheckCollisions(Tile tile, Player p1, Player p2, GameState state1, GameState state2, int invuln)
        {
                if ((tile.rectangle.Intersects(p1.Rect) && p1.State == GameState.Playing && (fileName == "Textures/tower" || fileName == "Textures/aliengreen")) || (p1.Rect.Bottom == 960 && p1.State == GameState.Playing))
                {
                    if (invuln == 0) //if the player has no invuln frames left, collision will kill them
                    {
                        p1.Alive = false;
                    }
                }
                if ((tile.rectangle.Intersects(p2.Rect) && p2.State == GameState.Playing && (fileName == "Textures/tower" || fileName == "Textures/aliengreen")) || (p2.Rect.Bottom == 960 && p2.State == GameState.Playing))
                {
                    p2.Alive = false;
                }
        }

        public void Update(Tile tile, Player p1, Player p2, GameState state1, GameState state2)
        {   
            if ((tile.rectangle.Intersects(p1.Rect) && p1.State == GameState.Playing && (fileName == "Textures/tower" || fileName == "Textures/aliengreen")) || (p1.Rect.Bottom >= 960 && p1.State == GameState.Playing))
            {
                p1.Alive = false; //if player 1 collides with the walls, they are not alive
            }
            if ((tile.rectangle.Intersects(p2.Rect) && p2.State == GameState.Playing && (fileName == "Textures/tower" || fileName == "Textures/aliengreen")) || (p2.Rect.Bottom >= 960 && p2.State == GameState.Playing))
            {
                p2.Alive = false; //if player 2 collides with the walls, they are not alive
            }
            if (tile.fileName == "Textures/aliengreen" )
            {
                if ((tile.rectangle.X + tile.SIZE) < p1.Rect.X && tile.passed == false) //passing a tower head will score a point
                {
                    if(p1.State == GameState.Playing)
                        p1.Score += 1;
                    if(p2.State == GameState.Playing)
                        p2.Score += 1;
                    tile.passed = true;

                }
            }
        }

        public void TileMove(Tile tile, Player p1, Player p2)
        {
            tile.rectangle.X -= (int)(2 + p1.Score / 3); //tile speed, affected by the player score
            if (p2.State == GameState.Playing && p2.Score > p1.Score) //if the 2nd player is in the lead
            {
                tile.rectangle.X -= (int)(2 + p2.Score / 3);
            }
            if (tile.rectangle.X <= -tile.SIZE) //tile wrap
            {
                tile.rectangle.X = 1440;
                tile.passed = false;
            }
        }

        public int X //properties
        {
            get
            {
               return rectangle.X;
            }
            set
            {
                rectangle.X = value;
            }
        }
        public int Y
        {
            get
            {
                return rectangle.Y;
            }
            set
            {
                rectangle.Y = value;
            }
        }
    }
}
