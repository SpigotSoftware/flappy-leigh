﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.IO;
using Microsoft.Xna.Framework.Audio;
#endregion

namespace FlappyLeigh
{
    enum GameState { Playing, Menu, GameOver, CharacterSelection, Instructions, MapSelection };
    //all potential states for the finite state machine 

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game //Carter, Dan, SeanFlappyLeigh.Game1
    {
        GraphicsDeviceManager graphics; 
        SpriteBatch spriteBatch;

        Texture2D textureBorder;

        Sounds soundObjects;

        Random rand1;
        bool mute;
        bool sfxMute;
        public bool shieldActive;
        public bool shieldInPlay;
        bool shieldUsed;
        bool shieldVisible;

        SoundEffectInstance backGroundNoiseInstance;

        Texture2D muteTexture;
        Texture2D unMuteTexture;
        Texture2D muteSFXTexture;
        Texture2D unMuteSFXTexture;

        double highScore = 0; 

        bool characterSelected;
        string mapSelection;
        bool twoPlayer;
        bool losingShield;

        Texture2D leighRaven; //leigh controls
        Rectangle leighRavenRectangle;

        Texture2D albirdo; //alberto controls
        Rectangle albirdoRectangle;

        Texture2D multiplayerSelection;
        Rectangle multiplayerSelectionRectangle;

        Texture2D shield;
        Rectangle shieldRectangle;
        Texture2D bonusPointsTexture;

        Rectangle screenSize;
        //so I do not need a new rectangle every game state
        Texture2D bubble;
        
        Player p1;
        Player p2;
        Obstacles o1; //test obstacle
        List<Tile> tiles;
        Vector2 tileOffset = new Vector2(10, 0);

        Texture2D menuState; //assorted textures
        Texture2D gameOverState;
        Texture2D playingState;
        Texture2D characterSelection;
        Texture2D instructionScreen;
        Texture2D Map;
        Texture2D Map1;
        Texture2D Map2;
        Texture2D Map3;
        Texture2D backGround;
        //Texture2D MapSelectionBackground;
        SpriteFont font;
        Powerup po1;
        int invuln;

        AddThree addThree1;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //whoever is in charge Game View should reset the game window to about 960x640 (30x20 tiles), or possibly 1440x960 (45 by 30)
            graphics.PreferredBackBufferWidth = 1440;	//32 * 45
            graphics.PreferredBackBufferHeight = 960;	//32 * 30
            Window.SetPosition(new Point(0,0));
            graphics.ApplyChanges();
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        { 
            // TODO: Add your initialization logic here
            rand1 = new Random();
            tiles = new List<Tile>();
            p1 = new Player("Player1", PlayerSprites.None, 1, true, 0); //create all the necessary assets
            p2 = new Player("Player2", PlayerSprites.None, 2, true, 0);
            p1.State = GameState.Menu; //"starting" state
            p2.State = GameState.Menu;
            soundObjects = new Sounds();
            shieldRectangle = new Rectangle(46, 0, 64, 64);
            screenSize = new Rectangle(0,0, GraphicsDevice.Viewport.Width * 2, GraphicsDevice.Viewport.Height * 2);
            albirdoRectangle = new Rectangle(200, 500, 300, 200);
            leighRavenRectangle = new Rectangle(1000, 500, 300, 200);
            multiplayerSelectionRectangle = new Rectangle(560, 500, 400, 250);
            shieldActive = false;
            shieldInPlay = false;
            shieldVisible = true;
            losingShield = false;
            shieldUsed = false;
            invuln = 0;
            po1 = new Powerup(ObstacleSprites.Shield);
            po1.Rect = new Rectangle(GraphicsDevice.Viewport.Width, 200, 64, 64);
            //multiplied by two seems to be the best way to fill the screen for some reason.
            addThree1 = new AddThree(ObstacleSprites.AddThree, new Rectangle(GraphicsDevice.Viewport.Width, 500, 60, 60));
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice); //load all of the textures
            font = this.Content.Load<SpriteFont>("SpriteFont1");
            playingState = this.Content.Load<Texture2D>("Textures/FlappyLeighBackground");
            backGround = this.Content.Load<Texture2D>("Textures/FlappyLeighBackground");
            gameOverState = this.Content.Load<Texture2D>("Textures/FlappyLeighGameOver");
            menuState = this.Content.Load<Texture2D>("Textures/FlappyLeighMenu");
            characterSelection = this.Content.Load<Texture2D>("Textures/CharacterSelection");
            instructionScreen = this.Content.Load<Texture2D>("Textures/InstructionScreen");
            textureBorder = this.Content.Load<Texture2D>("Textures/SelectionSquare");
            albirdo = this.Content.Load<Texture2D>("Textures/FlappyAlberto");
            //changed it so that it has all of these unloaded and then draws it depending on what state it is in.
            p1.Sprite = this.Content.Load<Texture2D>("Textures/FlappyAlberto"); 
            p1.Sprite = this.Content.Load<Texture2D>("Textures/FlappyLeigh");
            leighRaven = this.Content.Load<Texture2D>("Textures/FlappyLeigh");
            //flap = this.Content.Load<SoundEffect>("SoundFileName");
            soundObjects.backGroundNoise = this.Content.Load<SoundEffect>("BackGroundMusic"); //load background sound
            backGroundNoiseInstance = soundObjects.backGroundNoise.CreateInstance();
            multiplayerSelection = this.Content.Load<Texture2D>("Textures/MultiplayerSelection");
            muteTexture = this.Content.Load<Texture2D>("Textures/Mute");
            unMuteTexture = this.Content.Load<Texture2D>("Textures/UnMute");
            soundObjects.ugh = this.Content.Load<SoundEffect>("UGHW"); //leigh's glorious sound effects
            soundObjects.wtf = this.Content.Load<SoundEffect>("WTFW");
            soundObjects.yeah = this.Content.Load<SoundEffect>("YEAHW");
            soundObjects.laugh = this.Content.Load<SoundEffect>("LAUGHW");
            soundObjects.ughInstance = soundObjects.ugh.CreateInstance();
            soundObjects.wtfInstance = soundObjects.wtf.CreateInstance();
            soundObjects.yeahInstance = soundObjects.yeah.CreateInstance();
            soundObjects.laughInstance = soundObjects.laugh.CreateInstance();
            muteSFXTexture = this.Content.Load<Texture2D>("Textures/SFXMute");
            unMuteSFXTexture = this.Content.Load<Texture2D>("Textures/SFX");
            po1.Sprite = this.Content.Load<Texture2D>("Textures/Shield");
            Map = this.Content.Load<Texture2D>("MapSelection1");
            Map1 = this.Content.Load<Texture2D>("MapSelection2");
            Map2 = this.Content.Load<Texture2D>("MapSelection2");
            Map3 = this.Content.Load<Texture2D>("MapSelection3");
            //MapSelectionBackground = this.Content.Load<Texture2D>("Textures/MapSelectionBackground");
            mapSelection = "Content/Map.txt";
            bubble = this.Content.Load<Texture2D>("Textures/Bubble");
            bonusPointsTexture = this.Content.Load<Texture2D>("Textures/Points");
            
            // TODO: use this.Content to load your game content here
        }

        protected void LoadTiles() //tile loading method
        {
            Vector2 position = new Vector2();
            position = Vector2.Zero;

            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].fileName != "Textures/empty")
                {
                    int nIndex = -1;
                    for (int j = 0; j < i; j++)
                    {
                        if (tiles[j].fileName == tiles[i].fileName)
                        {
                            nIndex = j;
                            break;
                        }
                    }
                    if (nIndex == -1)
                        tiles[i].texture2D = this.Content.Load<Texture2D>(tiles[i].fileName);
                    else
                    {
                        tiles[i].texture2D = tiles[nIndex].texture2D;
                    }
                    tiles[i].rectangle.X = (int)position.X;
                    tiles[i].rectangle.Y = (int)position.Y;
                }
                position.X += tiles[i].SIZE;

                if (position.X >= (tiles[i].SIZE * 46)) //So we have an extra one on the edge of the screen
                {
                    position.X = 0;
                    position.Y += tiles[i].SIZE;
                }
            }
            //method is also used to restart the spawning of the tiles
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public void AliveCheck()
        {
            if (twoPlayer == true)
            {
                if (p2.Alive == true && p1.Alive == false)
                {
                    p1.State = GameState.GameOver;
                }

                else if (p1.Alive == true && p2.Alive == false)
                {
                    p2.State = GameState.GameOver;
                }
                else if (p1.Alive == false && p2.Alive == false)
                {
                    p1.State = GameState.GameOver;
                    p2.State = GameState.GameOver;
                }
            }
            else
            {
                p2.State = GameState.GameOver;
                if (p1.Alive == false)
                {
                    p1.State = GameState.GameOver;
                }
            }
        }

        public void ResetCharacter()
        {
            p1.Choice = PlayerSprites.None;
            p2.Choice = PlayerSprites.None;
            characterSelected = false;
            twoPlayer = false;
            mapSelection = "Content/Map.txt";
        }

        public void DrawMuteSFX()
        {
            if (sfxMute == true)
            {
                spriteBatch.Draw(muteSFXTexture, new Rectangle(1150, 0, 100, 100), Color.White);
            }
            else
            {
                spriteBatch.Draw(unMuteSFXTexture, new Rectangle(1150, 0, 100, 100), Color.White);
            }
        }

        public void DrawMute()
        {
            if (mute == true)
            {
                spriteBatch.Draw(muteTexture, new Rectangle(1200, 0, 100, 100), Color.White);
            }
            else
            {
                spriteBatch.Draw(unMuteTexture, new Rectangle(1200, 0, 100, 100), Color.White);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            p1.PrevKState = p1.KState;
            p1.KState = Keyboard.GetState();

            p2.PrevKState = p2.KState;
            p2.KState = Keyboard.GetState();

            backGroundNoiseInstance.Play();
            backGroundNoiseInstance.Volume = 0.2f;

            if (mute == false && (p1.SingleKeyPress(Keys.M)) && !p1.PrevKState.IsKeyDown(Keys.M))
            {
                backGroundNoiseInstance.Pause();
                mute = true;
            }
            else if (mute == true && (p1.SingleKeyPress(Keys.M)))
            {
                backGroundNoiseInstance.Stop();
                mute = false;
            }

            if (sfxMute == false && (p1.SingleKeyPress(Keys.K)) && !p1.PrevKState.IsKeyDown(Keys.K))
            {
                soundObjects.ughInstance.Volume = 0;
                soundObjects.yeahInstance.Volume = 0;
                soundObjects.laughInstance.Volume = 0;
                soundObjects.wtfInstance.Volume = 0;
                sfxMute = true;
            }
            else if (sfxMute == true && (p1.SingleKeyPress(Keys.K)))
            {
                soundObjects.ughInstance.Volume = 100;
                soundObjects.yeahInstance.Volume = 50;
                soundObjects.laughInstance.Volume = 100;
                soundObjects.wtfInstance.Volume = 100;
                sfxMute = false;
            }

            if(p1.State == GameState.Menu && p2.State == GameState.Menu)
            {
                characterSelected = false;
                //reset the tiles
                p1.Reset();
                p2.Reset();
                if (p1.SingleKeyPress(Keys.Enter))
                {
                    p1.State = GameState.Instructions;
                    p2.State = GameState.Instructions;
                    //make the state Playing from menu
                }
            }

            else if(p1.State == GameState.Instructions && p2.State == GameState.Instructions)
            {
                if (p1.SingleKeyPress(Keys.Enter))
                {
                    p1.State = GameState.CharacterSelection;
                    p2.State = GameState.CharacterSelection;
                }
            }

            else if(p1.State == GameState.CharacterSelection && p2.State == GameState.CharacterSelection)
            {
                p1.Reset();
                p2.Reset();
                if (p1.SingleKeyPress(Keys.D1) || p1.SingleKeyPress(Keys.NumPad1))
                {
                    p1.Choice = PlayerSprites.Alberto;
                    p1.Sprite = this.Content.Load<Texture2D>("Textures/FlappyAlberto");
                    soundObjects.ughInstance.Play();
                    characterSelected = true;
                    twoPlayer = false;
                }
                else if (p1.SingleKeyPress(Keys.D2) || p1.SingleKeyPress(Keys.NumPad2))
                {
                    p1.Choice = PlayerSprites.Leigh;
                    p1.Sprite = this.Content.Load<Texture2D>("Textures/FlappyLeigh");
                    soundObjects.yeahInstance.Play();
                    characterSelected = true;
                    twoPlayer = false;
                }
                else if (p1.SingleKeyPress(Keys.D3) || p1.SingleKeyPress(Keys.NumPad3))
                {
                    p1.Choice = PlayerSprites.Alberto;
                    p2.Choice = PlayerSprites.Leigh;
                    p1.Sprite = this.Content.Load<Texture2D>("Textures/FlappyAlberto");
                    p2.Sprite = this.Content.Load<Texture2D>("Textures/FlappyLeigh");
                    characterSelected = true;
                    twoPlayer = true;
                }

                if (characterSelected == true && p1.SingleKeyPress(Keys.Enter))
                {
                    p1.State = GameState.MapSelection;
                    p2.State = GameState.MapSelection;
                }
                if (twoPlayer == true && p1.SingleKeyPress(Keys.Enter))
                {
                    p1.State = GameState.MapSelection;
                    p2.State = GameState.MapSelection;
                }
            }

            else if (p1.State == GameState.MapSelection && p2.State == GameState.MapSelection)
            {
                if (p1.SingleKeyPress(Keys.D1) || p1.SingleKeyPress(Keys.NumPad1))
                    mapSelection = "Content/Map.txt";
                if (p1.SingleKeyPress(Keys.D2) || p1.SingleKeyPress(Keys.NumPad2))
                    mapSelection = "Content/Map1.txt";
                if (p1.SingleKeyPress(Keys.D3) || p1.SingleKeyPress(Keys.NumPad3))
                    mapSelection = "Content/Map2.txt";
                if (p1.SingleKeyPress(Keys.D4) || p1.SingleKeyPress(Keys.NumPad4))
                    mapSelection = "Content/Map3.txt";
                if (p1.SingleKeyPress(Keys.Enter))
                {
                    LoadMap(mapSelection);
                    LoadTiles();
                    p1.State = GameState.Playing;
                    p2.State = GameState.Playing;
                }
            }
            else if (p1.State == GameState.Playing || p2.State == GameState.Playing)
            {
                p1.Update();
                p2.Update();
                addThree1.AddThreeMove(addThree1, p1);

                if (addThree1.addThreeActive == true)
                {
                    addThree1.ScoreAddThree(p1, p2, addThree1);
                }
                if (!shieldActive)
                {
                    AliveCheck();
                }
                foreach (Tile tile in tiles)
                {
                    if (!shieldActive) //Will check for collisions against barrier tiles while the shield is not active
                    {
                        tile.CheckCollisions(tile, p1, p2, p1.State, p2.State, invuln);
                    }
                    tile.Update(tile, p1, p2, p1.State, p2.State); //Updates the movement of the tiles
                    if (shieldActive) //Will only check if the shield is active
                    {
                        p1.Alive = true;
                        if (tile.CheckIfWas(tile, p1, p1.State) && !p1.Rect.Intersects(shieldRectangle)) //Checks against collisions against tiles while shield is active, to disable it.
                        {
                            losingShield = true;
                            if (invuln == 0)
                            {
                                losingShield = false;
                                shieldActive = false;
                                shieldUsed = false;
                            }
                        }
                    }
                    tile.TileMove(tile, p1, p2);
                }
                p1.Timer += 0.006;
                p2.Timer += 0.006;
                
            }
            else if (p1.State == GameState.GameOver && p2.State == GameState.GameOver)
            {
                LoadTiles();
                addThree1.rectangle.X = 1500;
                if (p1.SingleKeyPress(Keys.S))
                {
                    p1.State = GameState.Menu;
                    p2.State = GameState.Menu;
                    ResetCharacter();
                    //make the state Menu from GameOver
                }
                if (p1.SingleKeyPress(Keys.R))
                {
                    p1.State = GameState.Playing;
                    p2.State = GameState.Playing;
                    p1.Reset();
                    p2.Reset();
                    //fix the "r" press problem
                }
                if (p1.SingleKeyPress(Keys.C))
                {
                    ResetCharacter();
                    p1.State = GameState.CharacterSelection;
                    p2.State = GameState.CharacterSelection;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            if (p1.State == GameState.Playing || p2.State == GameState.Playing)
            {
                spriteBatch.Draw(playingState, screenSize, Color.White);
                DrawMute();
                DrawMuteSFX();
            }

            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].fileName != "Textures/empty")
                {
                    spriteBatch.Draw(tiles[i].texture2D, tiles[i].rectangle, Color.White);
                }
            }
            if (twoPlayer == false)
            {
                spriteBatch.Draw(p1.Sprite, p1.Rect, Color.White);
            }
            if (twoPlayer == true && p2.State == GameState.Playing)
            {
                spriteBatch.Draw(p2.Sprite, p2.Rect, Color.White);
            }
            if (twoPlayer == true && p1.State == GameState.Playing)
            {
                spriteBatch.Draw(p1.Sprite, p1.Rect, Color.White);
            }
           
            if (addThree1.addThreeActive == true)
            {
                spriteBatch.Draw(bonusPointsTexture, addThree1.rectangle, Color.White);
            }

            if (invuln > 0 && losingShield == false)
            {
                spriteBatch.Draw(bubble, p1.Rect, Color.White);
            }
            if(p1.State == GameState.Menu && p2.State == GameState.Menu)
            {
                spriteBatch.Draw(menuState, screenSize, Color.White);
                DrawMute();
                DrawMuteSFX();
                p1.Reset();
                p2.Reset();
                spriteBatch.DrawString(font, "Special thanks to Samuel Root for the music!", new Vector2(480, 420), Color.Black, 0f, new Vector2(0,0), 0.3f, SpriteEffects.None, 0);
                //resets the score to zero
            }

            else if(p1.State == GameState.Instructions && p2.State == GameState.Instructions)
            {
                spriteBatch.Draw(instructionScreen, screenSize, Color.White);
                DrawMute();
                DrawMuteSFX();
            }

            else if (p1.State == GameState.MapSelection && p2.State == GameState.MapSelection)
            {
                //spriteBatch.Draw(MapSelectionBackground, screenSize, Color.White);
                spriteBatch.Draw(backGround, screenSize, Color.White);
                spriteBatch.DrawString(font, "Map Selection:", new Vector2(450, 200), Color.Black);
                spriteBatch.DrawString(font, "Map 1", new Vector2(220, 380), Color.Black);
                spriteBatch.DrawString(font, "Map 2", new Vector2(1000, 380), Color.Black);
                spriteBatch.DrawString(font, "Alecs'", new Vector2(610, 320), Color.Black);
                spriteBatch.DrawString(font, "Hardcore Map", new Vector2(480, 380), Color.Black);
                if (mapSelection != "Content/Map.txt")
                spriteBatch.Draw(textureBorder, new Rectangle(140, 500, 360, 260), Color.Black);
                if (mapSelection == "Content/Map.txt")
                    spriteBatch.Draw(textureBorder, new Rectangle(140, 500, 360, 260), Color.White);
                if (mapSelection != "Content/Map1.txt")
                    spriteBatch.Draw(textureBorder, new Rectangle(940, 500, 360, 260), Color.Black);
                if (mapSelection == "Content/Map1.txt")
                    spriteBatch.Draw(textureBorder, new Rectangle(940, 500, 360, 260), Color.White);
                if (mapSelection != "Content/Map3.txt")
                    spriteBatch.Draw(textureBorder, new Rectangle(540, 500, 360, 260), Color.Black);
                if (mapSelection == "Content/Map3.txt")
                    spriteBatch.Draw(textureBorder, new Rectangle(540, 500,360, 260), Color.White);
                spriteBatch.Draw(Map, new Rectangle(160, 520, 320, 220), Color.White);
                spriteBatch.Draw(Map1, new Rectangle(960, 520, 320, 220), Color.White);
                spriteBatch.Draw(Map3, new Rectangle(560, 520, 320, 220), Color.White);
            }

            else if (p1.State == GameState.CharacterSelection && p2.State == GameState.CharacterSelection)
            {
                spriteBatch.Draw(characterSelection, screenSize, Color.White);
                DrawMute();
                DrawMuteSFX();
                if (p1.Choice == PlayerSprites.Leigh && twoPlayer == false)
                {
                    spriteBatch.Draw(textureBorder, leighRavenRectangle, Color.White);
                }
                else if (p1.Choice == PlayerSprites.Alberto && twoPlayer == false)
                {
                    spriteBatch.Draw(textureBorder, albirdoRectangle, Color.White);
                }
                else if (twoPlayer == true)
                {
                    spriteBatch.Draw(textureBorder, multiplayerSelectionRectangle, Color.White);
                }
                spriteBatch.Draw(albirdo, albirdoRectangle, Color.White);
                spriteBatch.Draw(leighRaven, leighRavenRectangle, Color.White);
                spriteBatch.Draw(multiplayerSelection, new Rectangle(10, 30, 1600, 1200), Color.White);
            }

            else if ((p1.State == GameState.Playing || p2.State == GameState.Playing) && twoPlayer == true)
            {
                string tempMultiplayer = string.Format("Score: Player 1: {0} Player 2: {1}", p1.Score, p2.Score);
                spriteBatch.DrawString(font, tempMultiplayer, new Vector2((GraphicsDevice.Viewport.Width / 3), 0), Color.Firebrick, 0, Vector2.Zero, .3f, SpriteEffects.None, 0);
            }
            else if (p1.State == GameState.Playing || p1.Alive == true)
            {
                string tempSinglePlayer = string.Format("Score: {0}", p1.Score);
                spriteBatch.DrawString(font, tempSinglePlayer, new Vector2((GraphicsDevice.Viewport.Width / 2), 0), Color.Firebrick, 0, Vector2.Zero, .3f, SpriteEffects.None, 0);
            }

            else if (p1.State == GameState.GameOver && p2.State == GameState.GameOver)
            {
                if ((p1.Score + p1.BonusPoints) >= highScore)
                {
                    highScore = p1.Score;
                }
                if ((p2.Score + p2.BonusPoints) >= highScore)
                {
                    highScore = p2.Score;
                }
                shieldInPlay = false;
                invuln = 0;
                spriteBatch.Draw(gameOverState, screenSize, Color.White);
                string printHighScore = string.Format(highScore.ToString());
                spriteBatch.DrawString(font, "High Score: ", new Vector2(550, 750), Color.White);
                spriteBatch.DrawString(font, printHighScore, new Vector2(1000, 750), Color.White);

                //prints the high score, need to edit the size and location
                if (twoPlayer == false)
                {
                    spriteBatch.DrawString(font, "Your Score:" + (p1.Score + p1.BonusPoints).ToString(), new Vector2(530, 550), Color.White);
                }
                else
                {
                    spriteBatch.DrawString(font, "Player 1 Score: " + (p1.Score + p1.BonusPoints).ToString(), new Vector2(450, 450), Color.White);
                    spriteBatch.DrawString(font, "Player 2 Score: " + (p2.Score + p2.BonusPoints).ToString(), new Vector2(450, 550), Color.White);   
                }

                if ((p1.Score + p1.BonusPoints) > (p2.Score + p2.BonusPoints) && twoPlayer == true)
                {
                    string printWinner = string.Format("{0} wins!", p1.Name);
                    spriteBatch.DrawString(font, printWinner, new Vector2((GraphicsDevice.Viewport.Width / 2.6f), (50)), Color.White);
                    soundObjects.wtfInstance.Play();
                }
                else if ((p2.Score + p2.BonusPoints) > (p1.Score + p1.BonusPoints) && twoPlayer == true)
                {
                    string printWinner = string.Format("{0} wins!", p2.Name);
                    spriteBatch.DrawString(font, printWinner, new Vector2((GraphicsDevice.Viewport.Width / 2.6f), (50)), Color.White);
                    soundObjects.laughInstance.Play();
                }
                else if ((p2.Score + p2.BonusPoints) == (p1.Score + p1.BonusPoints) && twoPlayer == true)
                {
                    string printWinner = string.Format("Tie!");
                    spriteBatch.DrawString(font, printWinner, new Vector2((GraphicsDevice.Viewport.Width / 2), (50)), Color.White);
                }
                //prints the score of the round, need to edit size and location
            }

            if (p1.Score != 0 && p1.Alive == true && twoPlayer == false )
            {
                if (shieldInPlay == false && p1.Score % 1 == 0) //Places the shield icon on the screen if not already picked up.
                {
                    int i = rand1.Next(60, 960);
                    shieldRectangle.X = 32 * 47;
                    shieldRectangle.Y = i;
                    shieldInPlay = true;
                    shieldVisible = true;
                }
                shieldRectangle.X -= (int)(2 + p1.Score / 3); //Moves the shield pickup along the screen.
                if (shieldRectangle.X <= -shieldRectangle.Width)
                {
                    shieldInPlay = false;
                }
                
                if (losingShield == true) //Slowly takes away from the invuln int after colliding with a barrier with shields active. Once is at zero, the shield is no longer
                {                         //active
                    if (invuln > 0)
                    {
                        invuln = invuln - 1;
                    }
                    if (invuln == 0)
                    {
                        losingShield = false;
                        shieldUsed = false;
                    }
                }
                if (shieldRectangle.Intersects(p1.Rect))  //Sets the shield's active bool to true if the player collides with the pickup icon. Also sets the invuln
                {                                         //value based on how fast the player is moving.
                    shieldVisible = false;
                    shieldActive = true;
                    shieldUsed = true;
                    shieldRectangle.Y = -50;
                    if (p1.Score < 20)
                    {
                        if (p1.Score < 5)
                        {
                            invuln = 70;
                        }
                        else
                        {
                            invuln = 70 - p1.Score * 3;
                        }
                    }
                    else
                    {
                        invuln = 40;
                    }
                }
                if (shieldVisible == true && p1.Alive == true && shieldUsed == false) //Will only draw the shield pickup if it's not already in use
                {
                    spriteBatch.Draw(po1.Sprite, shieldRectangle, Color.White);
                }
                if (addThree1.addThreeActive == true)  //Will only draw the addThree pickup if not in use
                {
                    spriteBatch.Draw(bonusPointsTexture, addThree1.rectangle, Color.White);
                }

            }
            
            string stateTemp = string.Format("State: {0}, {1}", p1.State, p2.State); //to write the state for gameplay testing
           // spriteBatch.DrawString(font, stateTemp, new Vector2(0, 0), Color.Firebrick, 0, Vector2.Zero, .3f, SpriteEffects.None, 0);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void LoadMap(string map)
        {
            StreamReader file = null; //tile array generator
            try
            {
                if (tiles.Count > 0)
                    tiles.Clear();
                file = new StreamReader(map);
                string line = "";
                while (line != null)
                {
                    line = file.ReadLine();
                    for (int i = 0; i < line.Length; i++)
                    {
                        tiles.Add(new Tile(line[i]));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
    }
}
