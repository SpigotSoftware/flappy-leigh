﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using System.IO;

namespace FlappyLeigh
{
    enum PlayerSprites { Leigh, Alberto, None }
    class Player : GameObject //Carter, Dan
    {
        Vector2 playerVec;
        string name;
        PlayerSprites choice;
        double timer = 0;
        int score;
        GameState state;
        int number;
        bool alive = true;
        int bonusPoints;

        public Player(string nameInput, PlayerSprites spriteChoice, int pNumber, bool alive, int bonusPoints)
        {
            name = nameInput;
            rect = new Rectangle(50, 350, 64, 64);
            choice = spriteChoice;
            score = 0;
            timer = 0;
            number = pNumber;
            this.alive = alive;
            this.bonusPoints = bonusPoints;
        }

        public void Gravity()
        {
            if (choice == PlayerSprites.Alberto)
            {
                playerVec.Y = (float)((playerVec.Y) + (-5 * timer)); //Vf = Vi + a*del(t)
                rect.Y = rect.Y - (int)((playerVec.Y * timer) + (0.5 * -5 * timer * timer)); //del(s)= Vi*del(t) + 0.5*a*del(t)^2
            }
            else if (choice == PlayerSprites.Leigh)
            {
                playerVec.Y = (float)((playerVec.Y) + (-7 * timer)); //Vf = Vi + a*del(t)
                rect.Y = rect.Y - (int)((playerVec.Y * timer) + (0.5 * -5 * timer * timer)); //del(s)= Vi*del(t) + 0.5*a*del(t)^2
            }
        }

        public void Flap() //alberto flaps higher than leigh
        {
            if (choice == PlayerSprites.Alberto)
            {
                playerVec.Y = 30; //instantly set the Vec.Y so that the player will glide up
            }
            else if (choice == PlayerSprites.Leigh)
            {
                playerVec.Y = 25;
            }
        }

        public void Reset()
        {
            X = 30;
            Y = 375;
            //reset the value of the player position
            Timer = 0;
            playerVec.Y = 0;
            Gravity();
            score = 0;
            //resets the gravity and such so the player is not falling at the rate at the end of last game
            alive = true;
        }

        public override void Update()
        {
            if (Y <= 0)
            {
                Y = 5;
                PlayerVecY = 0;
                //if the player goes too high, set it to a specific value
            }
            switch(number)
            {
                case 1:
                    if (SingleKeyPress(Keys.Enter))
                    {
                        Flap();
                    }
                    break;
                case 2:
                    if (SingleKeyPress(Keys.Space))
                    {
                        Flap();
                    }
                    break;
                default:
                    break;
            }
            Gravity();
        }

        public PlayerSprites Choice //properties
        {
            get { return choice; }
            set { choice = value; }
        }

        public GameState State
        {
            get { return state; }
            set { state = value; }
        }

        public double Timer
        {
            get { return timer; }
            set { if ((value) >= 0.19) //maximum to keep gravity at a reasonable rate
                    timer = 0.19;
                  else
                    timer = value;
            }
        }

        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public float PlayerVecY
        {
            get { return playerVec.Y; }
            set { playerVec.Y = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public bool Alive
        {
            get { return alive; }
            set { alive = value; }
        }
        public int BonusPoints
        {
            get { return bonusPoints; }
            set { bonusPoints = value; }
        }
    }
}
